"use strict";
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
  mode:'production',
  entry: {
    "client":"./pages/client.js",
    "tieu-diem":"./pages/tieu-diem.js",
    "emagazine":"./pages/emagazine.js",

  },
  output: {
    path: __dirname+"/static",
    // filename: "static/bundle.js"
  },
  resolve: {
    extensions: [".js", ".marko"]
  },
  module: {
    rules: [
      {
        test: /\.marko$/,
        loader: "marko-loader"
      },
      // {
      //   test: /\.(less|css)$/,
      //   use: ExtractTextPlugin.extract({
      //     fallback: "style-loader",
      //     use: "css-loader!less-loader"
      //   })
      // },
      {
        test: /\.(less|css)$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            // options: {
            //   // you can specify a publicPath here
            //   // by default it use publicPath in webpackOptions.output
              // publicPath: './static/'
            // }
          },
          "css-loader",
          // 'postcss-loader',
          "less-loader"
        ]
      }
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: "[name].css",
      chunkFilename: "[id].css"
    })
  ]
};
